resource "digitalocean_domain" "netlouinternal" {
  name       = "netlouinternal.com"
  ip_address = "${digitalocean_droplet.wp_node.0.ipv4_address}"

}

resource "digitalocean_record" "vacationhomebooking" {
    domain = "${digitalocean_domain.netlouinternal.name}"
    type = "A"
    name = "${digitalocean_droplet.wp_node.10.name}"
    value = "${digitalocean_droplet.wp_node.10.ipv4_address}"
  
}
resource "digitalocean_record" "tmbtc3" {
    domain = "${digitalocean_domain.netlouinternal.name}"
    type = "A"
    name = "${digitalocean_droplet.wp_node.9.name}"
    value = "${digitalocean_droplet.wp_node.9.ipv4_address}"
  
}
resource "digitalocean_record" "vacationbythebeach" {
    domain = "${digitalocean_domain.netlouinternal.name}"
    type = "A"
    name = "${digitalocean_droplet.wp_node.8.name}"
    value = "${digitalocean_droplet.wp_node.8.ipv4_address}"
  
}
resource "digitalocean_record" "vacationbythemouse" {
    domain = "${digitalocean_domain.netlouinternal.name}"
    type = "A"
    name = "${digitalocean_droplet.wp_node.7.name}"
    value = "${digitalocean_droplet.wp_node.7.ipv4_address}"
  
}
resource "digitalocean_record" "headfororlando" {
    domain = "${digitalocean_domain.netlouinternal.name}"
    type = "A"
    name = "${digitalocean_droplet.wp_node.6.name}"
    value = "${digitalocean_droplet.wp_node.6.ipv4_address}"
  
}
resource "digitalocean_record" "ovronline" {
    domain = "${digitalocean_domain.netlouinternal.name}"
    type = "A"
    name = "${digitalocean_droplet.wp_node.5.name}"
    value = "${digitalocean_droplet.wp_node.5.ipv4_address}"
  
}
resource "digitalocean_record" "lostinthemagic" {
    domain = "${digitalocean_domain.netlouinternal.name}"
    type = "A"
    name = "${digitalocean_droplet.wp_node.4.name}"
    value = "${digitalocean_droplet.wp_node.4.ipv4_address}"
  
}
resource "digitalocean_record" "choosemyvacationhome" {
    domain = "${digitalocean_domain.netlouinternal.name}"
    type = "A"
    name = "${digitalocean_droplet.wp_node.3.name}"
    value = "${digitalocean_droplet.wp_node.3.ipv4_address}"
  
}
resource "digitalocean_record" "greatamericanvacations" {
    domain = "${digitalocean_domain.netlouinternal.name}"
    type = "A"
    name = "${digitalocean_droplet.wp_node.2.name}"
    value = "${digitalocean_droplet.wp_node.2.ipv4_address}"
  
}
resource "digitalocean_record" "floridastarvacations" {
    domain = "${digitalocean_domain.netlouinternal.name}"
    type = "A"
    name = "${digitalocean_droplet.wp_node.1.name}"
    value = "${digitalocean_droplet.wp_node.1.ipv4_address}"
  
}
resource "digitalocean_record" "adoreorlandovillas" {
    domain = "${digitalocean_domain.netlouinternal.name}"
    type = "A"
    name = "${digitalocean_droplet.wp_node.0.name}"
    value = "${digitalocean_droplet.wp_node.0.ipv4_address}"
  
}


resource "digitalocean_record" "vac-db" {
    domain = "${digitalocean_domain.netlouinternal.name}"
    type = "A"
    name = "vac-db"
    value = "159.203.146.165"
}
