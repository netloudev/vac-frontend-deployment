# Creating Tags for Load Balancer and Firewall Controls
resource "digitalocean_tag" "frontend_tag" {
  name = "PRODUCTION"
}

resource "digitalocean_tag" "project_tag" {
  name = "${var.project}"
}

# Creating Web Server Nodes
resource "digitalocean_droplet" "wp_node" {
  count              = "${var.node_count}"
  image              = "${var.image_slug}"
  name               = "${lookup(var.node_name, count.index)}"
  region             = "${var.region}"
  size               = "${var.node_size}"
  private_networking = true
  backups            = true
  monitoring         = true
  ssh_keys           = ["${split(",",var.keys)}"]
  user_data          = "${data.template_file.user_data.rendered}"
  tags               = ["${digitalocean_tag.frontend_tag.id}", "${digitalocean_tag.project_tag.id}"]

  lifecycle {
    create_before_destroy = true
  }

  connection {
    user        = "root"
    type        = "ssh"
    private_key = "${var.private_key_path}"
    timeout     = "2m"
  }
}

# Passing in user-data to set up Ansible user for configuration
data "template_file" "user_data" {
  template = "${file("config/cloud-config.yaml")}"

  vars {
    public_key   = "${var.public_key}"
    ansible_user = "${var.ansible_user}"
  }
}